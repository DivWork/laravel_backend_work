<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use JWTAuthException;
use Validator;
class UserController extends Controller
{
    private $user;
    public function __construct(User $user){
        $this->user = $user;
    }

    public function register(Request $request){
      // dd($request->get('name'));
      $validator = Validator::make($request->all(), [
					 'first_name' => 'required',
           'last_name' => 'required',
           'mobile' => 'required',
           'email' => 'required',
           'DOB' => 'required',
           'gender' => 'required',
					 'password' => 'required',
			 ]);
			 if ($validator->fails()) {
					 return Response::json($validator->messages(), 410);
			 }else{
         $gender = 0;
         if($request->get('gender') == 'Male' || $request->get('gender') == 'MALE' || $request->get('gender') == 'male'){
           $gender = 1;
         }
         $user = $this->user->create([
           'first_name' => $request->get('name'),
           'last_name' => $request->get('name'),
           'mobile'=> $request->get('mobile'),
           'email' => $request->get('email'),
           'DOB'=>$request->get('DOB'),
           'gender' => $request->get('gender'),
           'password' => bcrypt($request->get('password'))
         ]);
         return response()->json(['status'=>true,'message'=>'User created successfully','data'=>$user]);
       }
    }

    public function login(Request $request){
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['invalid_email_or_password'], 422);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }
        return response()->json(compact('token'));
    }
    public function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        return response()->json(['result' => $user]);
    }
}
